const {app, BrowserWindow} = require('electron');
const path = require('path');
const url = require('url');

const imageAssetsDir = path.join(__dirname, 'img');
const viewAssetsDir = path.join(__dirname, 'view');
  
  
this.window = undefined;

this.openView = (fileUri, callback = null, windowSettings = null) => {
    if(windowSettings == null){
        windowSettings = {}
    }
    
    if(windowSettings.height == undefined){
        windowSettings.height = 300;
    }

    if(windowSettings.width == undefined){
        windowSettings.width = 800;
    }

    if(windowSettings.frame == undefined){
        windowSettings.frame = false;
    }

    this.window = new BrowserWindow(windowSettings);

    this.window.setAlwaysOnTop(true, "floating", 1);

    // allows the window to show over a fullscreen window
    this.window.setVisibleOnAllWorkspaces(true);

    this.window.loadURL(url.format({
        pathname: path.join(__dirname, '../', fileUri),
        protocol: 'file:',
        slashes: true
    }))
    
    // Emitted when the window is closed.
    this.window.on('closed', function () {
        mainWindow = null
    });

    if(callback != null){
    this.window.webContents.on('did-finish-load', function() {
        return callback();
    });
    }

}

this.loadView = (fileUri, callback = null) => {
    if(this.window == null){
        return this.openView(fileUri, callback)
    }else{
        this.window.loadURL(url.format({
            pathname: path.join(__dirname,'../', fileUri),
            protocol: 'file:',
            slashes: true
        }))
    }
}
  
module.exports = this