const {app, BrowserWindow} = require('electron');
const path = require('path');
const url = require('url');

this.window = undefined;

this.openView = (fileUri, callback = null, windowSettings = null) => {
    if(windowSettings == null){
        windowSettings = {
            width: 1200, 
            height: 600,
            frame: true,
            'minWidth': 1024,
            'minHeight': 400,
            }
        }

    this.window = new BrowserWindow(windowSettings);

    this.window.loadURL(url.format({
        pathname: path.join(__dirname,'../', fileUri),
        protocol: 'file:',
        slashes: true
    }))
    
    // Emitted when the window is closed.
    this.window.on('closed', function () {
        mainWindow = null
    })

    if(callback != null){
        this.window.webContents.on('did-finish-load', function() {
            return callback();
        });
    }

}

this.loadView = (fileUri, callback = null) => {
    if(this.window == null){
        return this.openView(fileUri, callback)
    }else{
        this.window.loadURL(url.format({
            pathname: path.join(__dirname,'../', fileUri),
            protocol: 'file:',
            slashes: true
        }))
    }
}


module.exports = this;