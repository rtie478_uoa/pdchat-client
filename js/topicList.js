function populateTopicList(topics){
    $('#topic-list').html("");

    if(topics.length == 0){
        $('#topic-list').append('<li>No topics here sorry!</li>');
    }

    topics.forEach(function(topic) {
        $('#topic-list').append('<li><a href="#" onclick="topicButtonClick(this); return false;" data-topicid = "' + topic.id + '">' + topic.name + '</a></li>');
    }, this);
    // $('#guild-room-list').append('<li><a href="#" class="new-topic-button"><i class="fa fa-plus"></i> New</a></li>');
}

function topicButtonClick(button) {
    var topicid = $(button).data("topicid");
    
    ipcRenderer.send("open_topic", topicid);

    clearOldTopicView();
    //TODO, show loading screen...
    
    //Enable inputs
    $("#chat-send-button").prop("disabled", false);
    $("#chat-message-textarea").prop("disabled", false);
}


function displayTopic(topic){
    currentTopicId = topic.id;
    currentTopic = topic;
    
     //Send off new status update to the server.
     if(statusUpdatesEnabled){
        ipcRenderer.send("set_status", {
            status: "Online",
            mood: "",
            groupid: currentGuildId,
            topicid: currentTopicId
        });
    }
    
    clearOldTopicView();
    
    $("#chat-send-button").prop("disabled", false);
    $("#chat-message-textarea").prop("disabled", false);

    
    $('.display-topic-name').html(topic.name);
    $('.display-topic-attachment').html('<a href="#" class="topic-attachment-link" data-url="' + topic.attachment + '">' + topic.attachment + '</a>');
    
    //ADd subscribe / unsubscribe button
    if(topic.subscribed){
        $('.topic-subscription-control').html('<a class="topic-unsubscribe-button" href="#"><i class="fa fa-times-circle"></i> Unsubscribe</a>');
    }else{
        $('.topic-subscription-control').html('<a class="topic-subscribe-button" href="#"><i class="fa fa-plus-circle"></i> Subscribe</a>');
    }
    $('.topic-subscription-control').show();

    populateMemberList(topic.members);
    
    topic.messages.forEach((message) => {
        addMessage(message);
    });

    var chatPanel = $('#chat-messages');
    chatPanel.scrollTop(chatPanel.prop("scrollHeight"));

    

    //Clear notifications for this channel
    if(notifications[currentGuildId] != undefined && notifications[currentGuildId][topic.id] != undefined){
        delete notifications[currentGuildId][topic.id];
        if(Object.keys(notifications[currentGuildId]).length == 0){
            delete notifications[currentGuildId];
        }
    }

    //DONE
}

function clearOldTopicView(){
    //Clear old messages, topic data & guild list.
    $("#chat-messages-container").html("");
    $('.display-topic-name').html("");
    $('.display-topic-attachment').html("");
    $('#member-list').html("");
    
    clearActivityTarget();

    lastMessageBox = null;
    lastSender = "";
}


ipcRenderer.on("load_topic", (event, topic) => {
    displayTopic(topic);
});

$('body').on("click", '.new-topic-button', (e) => {
    e.preventDefault();
    ipcRenderer.send("show_new_topic_popup", currentGuildId);
});


$('body').on("click", '.topic-attachment-link', (e) => {
    e.preventDefault();
    console.log("TODO, show new topic popup / view?");
    var url = $(e.target).data("url");
    shell.openExternal(url);
});

$('body').on("click", '.search-topic-button', (e) => {
    e.preventDefault();
    ipcRenderer.send("open_topic_search_window");
});


$('body').on("click", '.topic-subscribe-button', (e) => {
    e.preventDefault();
    ipcRenderer.send("subscribe_user_to_topic", currentTopicId);
});

$('body').on("click", '.topic-unsubscribe-button', (e) => {
    e.preventDefault();
    ipcRenderer.send("unsubscribe_user_from_topic", currentTopicId);
});

//subscribed button changes to all button.
$('body').on("click", '.topic-filter-subscribed-button', (e) => {
    e.preventDefault();
    $('.filter-topic-action').html('<button class="topic-filter-all-button">All</button>');
    ipcRenderer.send("get_all_topics_for_group", currentGuildId);
});

//all button chnages to subscribed button.
$('body').on("click", '.topic-filter-all-button', (e) => {
    e.preventDefault();
    $('.filter-topic-action').html('<button class="topic-filter-subscribed-button">Subscribed</button>');
    ipcRenderer.send("get_subscribed_topics_for_group", currentGuildId);
});

ipcRenderer.on("populate_topics_list", (event, topics) => {
    populateTopicList(topics);
});