

var sentCount = 0;
$('#chat-send-button').click(function(event){
    var messageInput = $('#chat-message-textarea');
    var chatMessages = $('#chat-messages');
    var chatMessagesContainer = $('#chat-messages-container');

    //Check message field is not empty.
    if(messageInput.val() == ""){
        return;
    }

    console.log("sending message... " + sentCount++);

    var message = new Message(currentGuildId, currentTopicId, messageInput.val(), me.id);
    ipcRenderer.send("send_user_message", message);

    
    //Scroll the div to the new message.
    chatMessages.stop().animate({
        scrollTop: chatMessages[0].scrollHeight
    }, 800);

    //Clear the text field
    messageInput.val("");
});

function addMessage(message){
    var chatMessagesContainer = $('#chat-messages-container');

    if(lastSender == message.sender.id){
        lastMessageBox.find(".user-message-list").append("<li>" + message.content + "</li>");
    }else{
        //Append the message to the chat field //TEMP
        chatMessagesContainer.append("<hr>");

        //IF message from different user than last message
        var chatUserMessageBox = $(generateUserMessage());
        
        chatUserMessageBox.find(".sender-name-field").html(message.sender.name + " (" + message.sender.username + ")");
        chatUserMessageBox.find(".user-message-list").append("<li>" + message.content + "</li>");

        lastSender = message.sender.id;
        lastMessageBox = chatUserMessageBox;

        chatMessagesContainer.append(chatUserMessageBox);
    }
}
    
$('#chat-message-textarea').keypress(function(e){
    if(e.keyCode==13){
        $('#chat-send-button').click(); //trigger a click event on the send button
        e.preventDefault(); //Stop the newline character being created.
    }
});


function generateUserMessage(messageContent){
    var newMessageContainer = '<li class="user-message-box">' +
        '<div class="avatar-container">' +
            '<img src="../img/guild.png" />' +
        '</div>' +
        '<div style="float: left;">' + 
        '<div class="name-container">' +
        '    <span class="sender-name">' +
        '        <a href="#" class="sender-name-field"></a>' +
        '    </span>' +
        '</div>' +
        '<div class="user-messages">' +
        '    <ul class="user-message-list">' +
        '    </ul>' +
        '</div>' +
        '</div>' + 
        '<div style="clear: both;"></div>'
    '</li>';
    
    return newMessageContainer;
}