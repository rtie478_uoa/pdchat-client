function populateGuildList(guilds){
    $('#category-list').html("");
    guilds.forEach(function(guild) {
        $('#category-list').append('<li><a href="#" onclick="guildButtonClick(this); return false;" data-guildid = "' + guild.id + '">' + guild.name + '</a></li>');
    }, this);
}

function guildButtonClick(button) {
    var guildid = $(button).data("guildid");
    ipcRenderer.send("load_guild", guildid);
}

ipcRenderer.on("show_guild", (event, guild) => {
    displayGuild(guild);
})

function displayGuild(guildData){
    currentGuild = guildData;
    currentGuildId = guildData.id;
    clearOldView();

    $('.display-guild-name').html(guildData.name);
    $('.filter-topic-action').html('<button class="topic-filter-subscribed-button">Subscribed</button>');
    
    //Enable topic buttons
    $('.topic-filter-controls').show();
    $('.new-topic-button').show();
    $('.search-topic-button').show();

    populateTopicList(guildData.topics);
}

function clearOldView(){

    $('.topic-subscription-control').hide();

    //Disable inputs
    $("#chat-send-button").prop("disabled", true);
    $("#chat-message-textarea").prop("disabled", true);

    //Disable topic buttons
    $('.topic-filter-button').hide();
    $('.new-topic-button').hide();
    $('.search-topic-button').hide();
    
    //Clear old messages, topic data & guild list.
    $("#chat-messages-container").html("<--- Please select a topic");
    $('#guild-room-list').html("");
    $('.display-topic-name').html("");
    $('.display-topic-attachment').html("");
    $('#member-list').html("");
    lastMessageBox = null;
    lastSender = "";

    clearActivityTarget();
}

$('body').on("click", '.new-guild-button', (e) => {
    e.preventDefault();
    console.log("TODO, show new guild popup / view?");
    ipcRenderer.send("show_new_group_popup");
});

$('body').on("click", '#join-a-group-button', (e) => {
    e.preventDefault();
    ipcRenderer.send("show_join_group_popup");
});