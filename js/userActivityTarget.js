var target_users = {};
var target_selectedUser = null;

var innerCircleRadius = 0;
var outerCircleRadius = 0;

/**
 * Draw the target rings.
 */
function drawTarget(){
    var canvas = document.getElementById("myCanvas");
    

    var canvasCenterX = canvas.width / 2;
    var canvasCenterY = canvas.height / 2;

    var smallerMiddleValue = (canvasCenterX < canvasCenterY ? canvasCenterX : canvasCenterY);

    var ctx = canvas.getContext("2d");

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    ctx.strokeStyle = "#ffffff";

    //Inner circle
    innerCircleRadius = smallerMiddleValue - 50;
    ctx.beginPath();
    ctx.arc(canvasCenterX,canvasCenterY,innerCircleRadius, 0, 2*Math.PI);
    ctx.stroke();

    //Outer circle.
    outerCircleRadius = smallerMiddleValue - 20;
    ctx.beginPath();
    ctx.arc(canvasCenterX,canvasCenterY, outerCircleRadius, 0, 2*Math.PI);
    ctx.stroke();
}


function getMemberDotLocation(uid){
    var member = target_users[uid];
    var smallerMiddleValue = (canvasCenterX < canvasCenterY ? canvasCenterX : canvasCenterY);

    
}

/**
 * Add a user to the globasl target_users array.
 * @param {*} uid 
 * @param {*} activity 
 * @param {*} member 
 */
function addUser(uid, activity, member){
    target_users[uid] = {
        activity: activity,
        member: member,
        location: null
    };
}

function calculateUserPosition(uid){
     //Get the activtyUsers information from the temp global array.
     var activityUser = target_users[uid];
     
     var canvas = document.getElementById("myCanvas");

     //Calculate the center of the target.
     var canvasCenterX = canvas.width / 2;
     var canvasCenterY = canvas.height / 2;
 
     var distanceFromCenter = 0;
     //Calculate the distance from the center for this dot.
     //Note that the +/-5 adds padding so that the dot doesnt land on the line.
     if(activityUser.activity == ACTIVITY_SAME_TOPIC){
         //Anywhere from 0->20
         distanceFromCenter = randrange(0, innerCircleRadius - 5);
     }
     else if(activityUser.activity == ACTIVITY_TOPIC_MEMBER){
         distanceFromCenter = randrange(innerCircleRadius + 5 , outerCircleRadius - 5);
     }
     else if(activityUser.activity == ACTIVITY_UNKNOWN || activityUser.activity == ACTIVITY_OFFLINE){
         distanceFromCenter = randrange(0, outerCircleRadius + 5);
     }
 
 
     //Generate a random angle for this dot.
     var finalLocation = {
         distance: distanceFromCenter,
         angle : randrange(1, 360)
     }
     var attempts = 0;
     var locationTaken = true;
     while(attempts < 10 && locationTaken){
         //Assume that the location is not taken
         locationTaken = false;
 
         //Check if any other users have this exact location
         Object.keys(target_users).forEach((key) => {
             var targetUser = target_users[key];
             if(targetUser.location != null){
                 if(targetUser.location.distance == finalLocation.distance && targetUser.location.angle == finalLocation.angle){
                     locationTaken = true;
                 }
             }
         }, this);
 
         //If the location is taken, then generate a new angle and try again.
         if(locationTaken){
             finalLocation.angle = randrange(1, 360);
         }
     }
 
     //Calculate X, Y position based upon the finalLocation variables.
 
     finalLocation.x = canvasCenterX - finalLocation.distance * Math.cos(finalLocation.angle);
     finalLocation.y = canvasCenterY - finalLocation.distance * Math.sin(finalLocation.angle);

     target_users[uid].location = finalLocation;
}

/**
 * Draw a single user on the activity target given their UID
 * @param {*} uid 
 */
function drawUser(uid){
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");

    //Calculate the center of the target.
    var canvasCenterX = canvas.width / 2;
    var canvasCenterY = canvas.height / 2;

    var activityUser = target_users[uid];

    if(activityUser.location == null){
        calculateUserPosition(uid);
    }

    //Size of the users dot
    var size = 3;

    //If the user is the current logged in user, then change colour to green.
    ctx.fillStyle = "#ffffff";
    if(uid == me.id){
        ctx.fillStyle = "#00ff00";
    }
    
    //If the user is the currently selected user, then change colour to the users avatar colour.
    if(uid == target_selectedUser){
        size = 5;
        ctx.fillStyle = getAvatarColour(activityUser.member.id);
    }
    
    //Draw the dot.
    ctx.fillRect(activityUser.location.x - (size / 2), activityUser.location.y - (size / 2), size, size);
}

function randrange(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

/**
 * Draw all the users in the global target_users array.
 */
function drawUsers(){
    drawTarget();
    Object.keys(target_users).forEach((uid) => {
        drawUser(uid);
    });
}

/**
 * Redraws the users target with provided UID as highlighted.
 * @param {*} uid 
 */
function highlightUser(uid){
    target_selectedUser = uid;
    drawUsers();
}

/**
 * Removed all users from the activity target.
 */
function clearActivityTarget(){
    target_selectedUser = null;
    target_users = {};
    drawUsers();
}