const ACTIVITY_SAME_TOPIC   = 1001;
const ACTIVITY_UNKNOWN      = 1002;
const ACTIVITY_TOPIC_MEMBER = 1003; 
const ACTIVITY_OFFLINE      = 1004;


function addMember(member){
    addUser(member.id, getMemberActivity(member), member);
    $('#member-list').append(generateUserView(member.id));
    drawUsers();
}

function populateMemberList(members){
    members.forEach((member) => {
        addMember(member);
    });
}

function getAvatarColour(uid){
    var activity = target_users[uid].activity;
    var colour = "#fa0000";

    if(statusUpdatesEnabled && uid == me.id){
        //there is a delay in setting the users status to this topic, this just avoids that issue. Its rather hacky, but for protype it gets the idea across.
        //Only works when statusUpdatesEnabled is set to true, because otherwise no status update would be sent out regardless.
        return '#00ff00';
    }

    if(activity == ACTIVITY_OFFLINE){
        colour = '#fa0000';
    }
    
    if(activity == ACTIVITY_TOPIC_MEMBER){
        colour = '#ffff00';
    }
    
    if(activity == ACTIVITY_SAME_TOPIC){
        colour = '#00ff00';
    }
    return colour;
}

function getMemberActivity(member){
    if(member != null && member.status != undefined){
        if(member.status.topic != undefined && member.status.topic.id == currentTopicId){
            return ACTIVITY_SAME_TOPIC;
        }
        else if(member.isTopicMemeber != undefined && member.isTopicMemeber){
            return ACTIVITY_TOPIC_MEMBER;
        }
        else{
            return ACTIVITY_OFFLINE;
        }
    }
    return ACTIVITY_UNKNOWN;
}

function generateUserView(uid){
    var member = target_users[uid].member;
    var avatar_colour = getAvatarColour(uid);

    var nameToShow = member.name;
    if(member.id == me.id){
        nameToShow = "You";
    }

    var newView = '\
    <li class="member-list-item" data-uid="' + member.id + '">\
        <div class="row">\
            <div class="col-md-1 avatar" style="background-color: ' + avatar_colour + ';"></div>\
            <div class="col-md-10">\
                <span class="name">' + nameToShow + '</span><br>\
                <span class="status">' + member.status.status + '</span>\
            </div>\
        </div>\
    </li>\
    ';

    return newView;
}

$('body').on("mouseenter", ".member-list-item", (e) => {
    var listItem = $(e.target).closest(".member-list-item");
    var uid = listItem.data("uid");
    highlightUser(uid);
});

$('body').on("mouseleave", ".member-list-item", (e) => {
    target_selectedUser = null;
    drawUsers();
});