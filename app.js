const {app, BrowserWindow, globalShortcut, ipcMain, Tray} = require('electron');

const path = require('path');
const url = require('url');

const imageAssetsDir = path.join(__dirname, 'img');
const viewAssetsDir = path.join(__dirname, 'view');

const pdChat = require('./pdchat');
const Message = require('./pdchat/Message');

const mainWindow = require('./windows/mainWindow');
const popupWindow = require('./windows/popupWindow');

const accountController = require('./controllers/account');
const groupController = require('./controllers/group');
const messageController = require('./controllers/message');
const topicController = require('./controllers/topic');
const userController = require('./controllers/user');
const windowController = require('./controllers/window');
const statusController = require('./controllers/status');



app.on("ready", () => {
  //Open the main window
  mainWindow.openView("view/login.html");

  //Register shortcuts
  
  //Open shortcut status update dialog
  const statusUpdateShortcut = globalShortcut.register('Alt+/', () => {
    console.log('Alt+/ is pressed');
    popupWindow.openView('view/setStatusPopup.html', null, {
      width: 1024,
      height: 500
    });
  });

  //Open topic search dialog.
  const findTopicShortcut = globalShortcut.register('Alt+Shift+/', () => {
    console.log('Alt+Shift+/ is pressed');
    popupWindow.openView('view/findTopicPopup.html');
  });
  
  // //Open main window
  // const mainWindowShortcut = globalShortcut.register('CmdOrCtrl+/', () => {
  //   console.log('CmdOrCtrl+/ is pressed');
  //   openMainWindow();
  // });
    
  //Open dev tools in main window.
  const mainWindowDevToolsShortcut = globalShortcut.register('CmdOrCtrl+.', () => {
    if(mainWindow.window){
      mainWindow.window.webContents.openDevTools();
    }
  });
  //Open dev tools in popup window.
  const popupWindowDevToolsShortcut = globalShortcut.register('CmdOrCtrl+,', () => {
    if(popupWindow.window){
      popupWindow.window.webContents.openDevTools();
    }
  });

  //Log to console if shortcuts registered
  console.log("Alt + / is registered? ", globalShortcut.isRegistered('Alt+/'));
  console.log("Alt + Shift + / is registered? ", globalShortcut.isRegistered('Alt+Shift+/'));
  console.log("CmdOrCtrl + / is registered? ", globalShortcut.isRegistered('CmdOrCtrl+/'));
});

//Window & app Event Handles

//When the app is going to quit, deregister shortcuts
app.on('will-quit', () => {
  // Unregister all shortcuts.
  globalShortcut.unregisterAll()
});

//Close Diaglog popup windows if they lose focus
app.on('browser-window-blur', (event, window) => {
  if(window == popupWindow.window) window.close();
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow.window === null) {
    mainWindow.openView("view/index.html");
  }
})


pdChat.on("message", (message) => {
  if(mainWindow.window != null){
    mainWindow.window.webContents.send('incoming_message', message);
  }else{
    console.log("Main window is not open?");
    //TODO play the bing sound, such that when the user knows a message has come through even though the window is closed!
  }
});