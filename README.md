# PD Chat Client
The prototype chat client for Ryan Peter Tiedemanns Dissertation. This prototype is implemented as an Electron app written in the Node.js runtime.

# Install & Run
1. Clone this repo to a local directory.
2. Open a terminal in the local directory containg this project.
3. `npm install`
4. Edit the remote server addresses, found at the locations listed below in this readme.
5. `electron .`

# Credits
- Ryan Tiedemann <rtie478@aucklanduni.ac.nz>

# Modifications required to run local instance
To run this client and connect to your local server instance, you will need to edit the remote server addresses located in the following files:
- pdchat/pdchat.js
- pdchat/notificationSocket.js