const {ipcMain} = require('electron');
const pdChat = require('../pdchat');
const mainWindow = require('../windows/mainWindow');
const popupWindow = require('../windows/popupWindow');

ipcMain.on('attempt_login', (event, username, password) => {  
    pdChat.attemptLogin(username, password, (err, success) => {
      if(err) return event.sender.send("failed_login", "Failed to login!");
      mainWindow.loadView("view/index.html");
    });
  });
  
  ipcMain.on("attempt_register", (event, username, name, password) => {
    pdChat.registerUser({
      username: username,
      name: name,
      password: password
    }, (err, topic) => {
      if(err) return event.sender.send("failed_registration", "Failed to register new user :/");
      mainWindow.loadView("view/login.html");
    });
  });