  
const {ipcMain} = require('electron');
const pdChat = require('../pdchat');
const mainWindow = require('../windows/mainWindow');
const popupWindow = require('../windows/popupWindow');

  ipcMain.on("load_user", (event, userId) => {
    pdChat.getMemeberDetails(userId, (err, user) => {
      event.sender.send("loaded_user_detail", user);
    });
  });
  
  ipcMain.on("get_me", (event) => {
    pdChat.getMe((err, me) => {
      if(err) return console.log(err);
      event.sender.send("get_me_response", me);
    });
  });