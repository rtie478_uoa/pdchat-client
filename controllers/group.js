const {ipcMain} = require('electron');
const pdChat = require('../pdchat');
const mainWindow = require('../windows/mainWindow');
const popupWindow = require('../windows/popupWindow');

ipcMain.on("get_all_guilds", (event) => {
  pdChat.getAllGuilds((err, guilds) => {
    event.sender.send("get_all_guilds_response", guilds);
  });
});

ipcMain.on("get_guilds", (event) => {
    pdChat.getMemebersGuilds((err, guilds) => {
      event.sender.send("show_guilds", guilds);
    });
  });
  
  ipcMain.on("load_guild", (event, guildId) => {
    pdChat.loadGuildForMember(guildId, (err, guild) => {
      mainWindow.window.webContents.send("show_guild", guild);
    });
  });

  ipcMain.on("show_new_group_popup", (event) => {
    popupWindow.openView('view/newGroup.html');
  });

  ipcMain.on("new_group", (event, newGroup) => {
    console.log("Attempting to create new group with server");
    pdChat.newGroup(newGroup, (err, newId) => {
      if(err) return event.sender.send("new_group_failed", "Failed to create new group :/");
      console.log("New group created sussessfully! ID: " + newId)
      popupWindow.window.close();
      mainWindow.window.reload();

    });
  });

  ipcMain.on("join_group", (event, groupid) => {
    console.log("Attempting to join group: " + groupid)
    pdChat.joinGroup(groupid, (err, result) => {
      pdChat.getMemebersGuilds((err, guilds) => {
        mainWindow.window.webContents.send("show_guilds", guilds);
      });
    })
  });