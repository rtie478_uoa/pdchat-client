const {ipcMain} = require('electron');
const pdChat = require('../pdchat');
const mainWindow = require('../windows/mainWindow');
const popupWindow = require('../windows/popupWindow');

ipcMain.on('open_topic_search_window', (event) => {  
    popupWindow.openView('view/findTopicPopup.html');   
});


ipcMain.on('show_join_group_popup', (event) => {  
    popupWindow.openView('view/joinGroup.html');   
});

ipcMain.on('open_set_status_window', (event) => {  
    popupWindow.openView('view/setStatusPopup.html', null, {
        width: 1024,
        height: 500
      });  
});