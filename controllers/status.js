const {ipcMain} = require('electron');
const pdChat = require('../pdchat');
const mainWindow = require('../windows/mainWindow');
const popupWindow = require('../windows/popupWindow');

ipcMain.on("set_status", (event, newStatus, manuallySet = false) => {
    console.log("Attempting to update user status....");
    pdChat.sendStatusUpdate(newStatus, (err, success) => {
        if(err){
            console.log("Failed to update user status!");
            console.log(err);
            event.sender.send("set_status_failed");
        }else{
            console.log("User status updated!");
            event.sender.send("set_status_successful");
        }
        if(manuallySet){
            mainWindow.window.webContents.send("set_status_update_state", false);
        }
    });
});


ipcMain.on("get_set_status_screen_details", (event) => {
    pdChat.getMe((err, me) => {
        pdChat.getTopics((err, topics) => {
            pdChat.getAllGuilds((err, groups) => {
              var data = {
                me: me,
                topics: topics,
                groups: groups
              };
              event.sender.send("get_set_status_screen_details_response", data);
            })
        })
    })
});