const {ipcMain} = require('electron');
const pdChat = require('../pdchat');
const mainWindow = require('../windows/mainWindow');
const popupWindow = require('../windows/popupWindow');
const Message = require('../pdchat/Message')

ipcMain.on("send_user_message", (event, message) => {
    var m = Message.parse(message);
    pdChat.sendUserMessage(m, (err) => {
      if(err) return event.sender.send("send_user_message_failed", err);
      return event.sender.send("send_user_message_successful");
    });
  });