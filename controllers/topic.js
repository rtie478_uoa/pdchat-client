const {ipcMain} = require('electron');
const pdChat = require('../pdchat');
const mainWindow = require('../windows/mainWindow');
const popupWindow = require('../windows/popupWindow');

ipcMain.on("open_topic", (event, topicid) => {
    pdChat.loadTopic(topicid, (err, topic) => {
      mainWindow.window.webContents.send("load_topic", topic);
   });
 });

 ipcMain.on("new_topic", (event, newTopic) => {
  console.log("Attempting to create new topic with server");
  pdChat.newTopic(newTopic, (err, newId) => {
    if(err) return event.sender.send("new_topic_failed", "Failed to create new topic :/");
    console.log("New topic created sussessfully! ID: " + newId)
    popupWindow.window.close();
    mainWindow.window.reload();

    //When reload finished
    mainWindow.window.on("did-finish-load", () => {

      pdChat.loadTopic(newId, (err, topic) => {
        pdChat.loadGuildForMember(topic.group.id, (err, group) => {
          mainWindow.window.webContents.send("show_guild", group);
          mainWindow.window.webContents.send("load_topic", topic);
        })
      });

    });
  });
 });
  
ipcMain.on("open_guild_topic", (event, topicid) => {
    pdChat.loadTopic(topicid, (err, topic) => {
      pdChat.loadGuildForMember(topic.group.id, (err, group) => {
        mainWindow.window.webContents.send("show_guild", group);
        mainWindow.window.webContents.send("load_topic", topic);
      })
    });
});

ipcMain.on("get_my_topics", (event) => {
    pdChat.loadUserTopics(pdChat.me.id, (err, topics) => {
      if(err) return console.log(err);
      event.sender.send("get_my_topics_response", topics);
    });
  });

  ipcMain.on("get_topics", (event) => {
    pdChat.getTopics((err, topics) => {
      if(err) return console.log(err)
      event.sender.send("get_topics_response", topics);
    });
  });


  ipcMain.on("get_subscribed_topics_for_group", (event, groupid) => {
    pdChat.getMembersGuildTopics(groupid, (err, topics) => {
      if(err) return console.log(err)
      event.sender.send("populate_topics_list", topics);
    });
  });
  
  ipcMain.on("get_all_topics_for_group", (event, groupid) => {
    pdChat.loadGuild(groupid, (err, guild) => {
      if(err) return console.log(err)
      event.sender.send("populate_topics_list", guild.topics);
    });
  });


  
  ipcMain.on("show_new_topic_popup", (event, groupid) => {
    pdChat.loadGuild(groupid, (err, group) => {
      popupWindow.openView('view/newTopic.html', () => {
        popupWindow.window.webContents.send("populate_group", group);
      });
    });
  });

  ipcMain.on("subscribe_user_to_topic", (event, topicid) => {
    console.log("Going to subscribe user to topic: " + topicid);
    pdChat.subscribeTo(topicid, (err) => {
      if(err) return console.log(err);

      //Now, we have successfully registered, so refresh the topic view.
      pdChat.loadTopic(topicid, (err, topic) => {
        if(err) return console.log(err);
        mainWindow.window.webContents.send("load_topic", topic);
      })
    })
  });

  ipcMain.on("unsubscribe_user_from_topic", (event, topicid) => {
    console.log("Going to unsubscribing user from topic: " + topicid);
    pdChat.unsubscribeFrom(topicid, (err) => {
      if(err) return console.log(err);

      //Now, we have successfully registered, so refresh the topic view.
      pdChat.loadTopic(topicid, (err, topic) => {
        if(err) return console.log(err);
        mainWindow.window.webContents.send("load_topic", topic);
      })
    })
  });



  ipcMain.on("get_topics_and_groups", (event) => {
    pdChat.getTopics((err, topics) => {
      pdChat.getAllGuilds((err, groups) => {
        var data = {
          topics: topics,
          groups: groups
        };
        event.sender.send("get_topics_and_groups_response", data);
      })
    })
  })