const WebSocket = require('ws');
const urlAddress = "pdchat.xyz";

this.connection = null;
this.reconnectionTask = null;
this.user = null;

var handlers = {
    open: undefined,
    close: undefined,
    message: undefined
};

this.login = (user, callback = null) => {
    this.user = user;
    this.connection = new WebSocket('ws://' + urlAddress + ':8028/test/chat/' + user.id);
    this.registerSocketListeners();
    if(callback != null) return callback(null, true);
}

this.attemptReconnect = () => {
    this.login(this.user);
}


this.registerSocketListeners = () => {
    this.connection.on('open', function open() {
        console.log('Established connection to server!');

        this.reconnectionTask = null;

        if(handlers.open != undefined){
            handlers.open();
        }
    });

    this.connection.on("message", (data) => {
        if(handlers.message != undefined){
            handlers.message(data);
        }
    });

    this.connection.on("close", () => {
        console.log("No connection to websocket!");
        this.reconnectionTask = setTimeout(this.attemptReconnect, 10000);
       
        if(handlers.close != undefined){
            handlers.close();
        }
    });

    this.connection.on("error", (e) => {
        console.log("Socket Error: " + e);
    });
}


this.on = (handle, handler) => {
    handlers[handle] = handler;
}

module.exports = this;