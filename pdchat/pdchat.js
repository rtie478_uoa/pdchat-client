const WebSocket = require('ws');
const request = require('request');

const Message = require('./Message');

const urlAddress = "pdchat.xyz";

const notificationSocket = require('./notificationSocket');

this.requestSettings = {
    auth:{
        user: "",
        pass: ""
    }
}

//Details about logged in user.
this.me = {};
var me_last_update = 0; //Timestamp fo-r the last time me was updated!

//TODO, request the user if not set?
this.getMe = (callback) => {
    if(Date.now() - me_last_update > 0){
        this.loadUser(this.me.id, (err, me) => {
            if(err) return console.log("Failed to update me when expired!");
            this.me = me;
            this.me_last_update = Date.now();
            return callback(null, this.me);
        });
    }
    else{
        return callback(null, this.me);
    }
}

this.sendStatusUpdate = (status, callback) => {
    var options = {
        uri: "http://" + urlAddress + ":8040/user/" + this.me.id + "/setstatus",
        method: "PUT",
        body: status,
        json: true, 
        auth: {
            user: this.requestSettings.auth.user,
            pass: this.requestSettings.auth.pass
        }
    };
    request(options, (err, res, body) => {
        if(err) return calllback(err)
        if(res.statusCode == 200){
            return callback(null, true);
        }
        return callback(new Error("Failed to set user status"));
    });
}

this.loadUser = (userid, callback) => {
    request("http://" + urlAddress + ":8040/user/" + userid, this.requestSettings, (err, res, body) => {
        if(err) return callback(err)
        if(res.statusCode == 200){
            return callback(null, JSON.parse(body));
        }
        return callback(new Error("Failed to load user"));
    });
}

this.loadUserTopics = (userid, callback) => {
    request("http://" + urlAddress + ":8040/user/" + userid + "/topics", this.requestSettings, (err, res, body) => {
        if(err) return callback(err)
        if(res.statusCode == 200){
            return callback(null, JSON.parse(body));
        }
        return callback(new Error("Failed to load user topics"));
    });
}

this.attemptLogin = (username, password, callback) => {
    //Attempt login
    //Currentlly does not actually take a password, just the username is used to identify users.
    var loginSettings = {
        auth:{
            user: username,
            pass: password
        }
    }
    request("http://" + urlAddress + ":8040/user/me", loginSettings, 
        (err, res, body) => {
        console.log(body);
        if(err) return callback(err)
        console.log(res.statusCode)
        if(res.statusCode == 200){
            this.requestSettings = loginSettings;
            this.me = JSON.parse(body);
            this.me_last_update = Date.now();
            notificationSocket.login(this.me, (err) => {
                if(err) return console.log(err);
            });
            return callback(null, true);
        }
        return callback(new Error("Login failed!"));
    });
}

this.registerUser = (newUser, callback) => {
    var options = {
        uri: "http://" + urlAddress + ":8040/user",
        method: "PUT",
        body: newUser,
        json: true, 
    };
    request(options, (err, res, body) => {
        if(err) return calllback(err)
        if(res.statusCode == 200){
            return callback(null, true);
        }
        return callback(new Error("Failed"));
    });
}

this.getTopics = (callback) => {
    request('http://' + urlAddress + ':8040/topic/', this.requestSettings, function (error, response, body) {
        if(error) return callback(error);
        callback(null, JSON.parse(body));
    });
}

this.loadTopic = (topicId, callback) => {
    request('http://' + urlAddress + ':8040/topic/' + topicId, this.requestSettings, function (error, response, body) {
        if(error) return callback(error);
        callback(null, JSON.parse(body));
        });
}

this.loadGuild = (guildId, callback) => {
    request('http://' + urlAddress + ":8040/group/" + guildId, this.requestSettings, (err, resp, body) => {
        if(err) return callback(err)
        callback(null, JSON.parse(body));
    });
}

this.loadGuildForMember = (groupid, callback) => {
    this.getMe((err, me) => {
        if(me == undefined){
            return callback(new Error("Me not defined"));
        } 
        var found = false;
        this.me.groups.forEach(function(group) {
            if(group.id == groupid){
                found = true;
                return callback(null, group);
            }
        }, this);
        if(!found) return callback(new Error("Failed to find group in users group list"));
    })
}

this.joinGroup = (groupid, callback) => {
    var options = {
        uri: "http://" + urlAddress + ":8040/group/" + groupid + "/add/" + this.me.id,
        method: "POST",
        auth: {
            user: this.requestSettings.auth.user,
            pass: this.requestSettings.auth.pass
        }
    };
    request(options, (err, res, body) => {
        if(err) return callback(err);
        if(res.statusCode == 200){
            if(body != null && body.id != undefined) return callback(null, body.id);
            return callback(null, null);
        }
        return callback(new Error("Failed to join group"));
    });
}

this.getAllGuilds = (callback) => {
    request('http://' + urlAddress + ":8040/group/", this.requestSettings, (err, resp, body) => {
        if(err) return callback(err)
        callback(null, JSON.parse(body));
    });
}

this.getMemebersGuilds = (callback) => {
    this.getMe((err, me) => {
        if(this.me.groups == undefined){
            return callback(new Error("Me not defined"));
        }
        return callback(null, this.me.groups);
    })
}

this.getMembersGuildTopics = (groupid, callback) => {
    this.getMe((err, me) => {
        if(err) return callback(err);
        if(this.me.groups == undefined) return callback(new Error("Me not defined"));
        var found = false;
        this.me.groups.forEach(function(group) {
            if(group.id == groupid){
                found = true;
                return callback(null, group.topics);
            }
        }, this);
        if(!found) return callback(new Error("Failed to find group in users group list"));
    })
}

this.members = {};
this.getMemeberDetails = (memberId, callback) => {
    if(this.members[memberId] == undefined) {
        request('http://' + urlAddress + ":8040/user/" + memberId, function(err, response, body){
            if(err) return callback(err)
            members[memberId] = JSON.parse(body);
            return callback(null, members[memberId]);
        })
    }
    else{
        return callback(null, members[memberId]);
    }
}

this.newTopic = (newTopic, callback) => {
    var options = {
        uri: "http://" + urlAddress + ":8040/topic/",
        method: "PUT",
        body: newTopic,
        json: true,
        auth: {
            user: this.requestSettings.auth.user,
            pass: this.requestSettings.auth.pass
        }
    };
    request(options, (err, res, body) => {
        if(err) return callback(err);
        if(res.statusCode == 200){
            if(body != null && body.id != undefined) return callback(null, body.id);
            return callback(null, null);
        }
        return callback(new Error("Failed to create new topic"));
    });
}

this.newGroup = (newGroup, callback) => {
    var options = {
        uri: "http://" + urlAddress + ":8040/group/",
        method: "PUT",
        body: newGroup,
        json: true,
        auth: {
            user: this.requestSettings.auth.user,
            pass: this.requestSettings.auth.pass
        }
    };
    request(options, (err, res, body) => {
        if(err) return callback(err);
        if(res.statusCode == 200){
            if(body != null && body.id != undefined) return callback(null, body.id);
            return callback(null, null);
        }
        return callback(new Error("Failed to create new group"));
    });
}

this.subscribeTo = (topicid, callback) => {
    var options = {
        uri: "http://" + urlAddress + ":8040/topic/" + topicid + "/subscribe/" + this.me.id,
        method: "POST",
        auth: {
            user: this.requestSettings.auth.user,
            pass: this.requestSettings.auth.pass
        }
    };
    request(options, (err, res, body) => {
        if(err) return callback(err);
        if(res.statusCode == 200){
            return callback(null, true);
        }
        return callback(new Error("Failed to subscribe user"))
    });
}

this.unsubscribeFrom = (topicid, callback) => {
    var options = {
        uri: "http://" + urlAddress + ":8040/topic/" + topicid + "/unsubscribe/" + this.me.id,
        method: "DELETE",
        auth: {
            user: this.requestSettings.auth.user,
            pass: this.requestSettings.auth.pass
        }
    };
    request(options, (err, res, body) => {
        if(err) return callback(err);
        if(res.statusCode == 200){
            return callback(null, true);
        }
        return callback(new Error("Failed to unsubscribe user"))
    });
}


/**
 * Sends a message on behalf of the user.
 * @param {Message} m Message Object to be send to the pdChat server
 */
this.sendUserMessage = (m, callback) => {
    notificationSocket.connection.send(m.toJson(), (err) => {
        if(err) return callback(err);
        return callback(null);
    });
}

this.on = (handle, handler) => {
    notificationSocket.on(handle, handler);
}
    

module.exports = this;