function Message(guild, topic, payload, senderId) {
    this.guild_id = guild;
    this.topic_id = topic;
    this.content = payload;
    this.timestamp = null;
    this.sender_id = senderId;

    this.toJson = () => {
        return JSON.stringify(this);
    }
}

module.exports = Message;
module.exports.parse = (object) => {
    var m = new Message(object.guild, object.topic, object.message);
    Object.keys(object).forEach((key) => {
        m[key] = object[key];
    });
    return m;
}